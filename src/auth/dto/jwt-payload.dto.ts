import { Role } from '../../user/entities/role.entity';

export class JwtPayload {
  email: string;
  roles: Role[];
}
