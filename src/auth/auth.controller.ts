import { Body, Controller, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserDTO } from '../user/dto/user.dto';
import { LoginDTO } from '../user/dto/login.dto';
import { UserRO } from '../user/dto/userRO.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  @UsePipes(new ValidationPipe())
  async register(@Body() data: UserDTO): Promise<UserRO> {
    return this.authService.register(data);
  }

  @Post('login')
  @UsePipes(new ValidationPipe())
  async login(@Body() data: LoginDTO): Promise<UserRO> {
    return this.authService.login(data);
  }
}
