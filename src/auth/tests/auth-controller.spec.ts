import { AuthService } from '../auth.service';
import { Test, TestingModule } from '@nestjs/testing';
import { UserDTO } from '../../user/dto/user.dto';
import { UserRO } from '../../user/dto/userRO.dto';
import { LoginDTO } from '../../user/dto/login.dto';
import { AuthController } from '../auth.controller';
jest.mock('../auth.controller');

describe('AuthController', () => {
  let authController: AuthController;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [
        AuthController,
      ],
    }).compile();
    authController = module.get(AuthController);
  });

  it('should register and respond with userRO', async () => {
    const user: UserDTO = {
      login: 'adrianadrian',
      email: 'adrian@gmail.com',
      password: 'testtest',
    };
    const result: UserRO = {
      login: 'adrianadrian',
      email: 'adrian@gmail.com',
      created: new Date(Date.now()),
      lastLogin: null,
      roles: [{id: 0, name: 'USER'}],
    };
    jest.spyOn(authController, 'register').mockResolvedValue(result);
    expect(await authController.register(user)).toEqual(result);
  });

  it('should login and respond with userRO and token', async () => {
    const user: LoginDTO = {
      login: 'adrianadrian',
      password: 'testtest',
    };
    const result: UserRO = {
      login: 'adrianadrian',
      email: 'adrian@gmail.com',
      created: new Date(Date.now()),
      lastLogin: null,
      roles: [{id: 0, name: 'USER'}],
      token: 'testtoken',
    };
    jest.spyOn(authController, 'login').mockResolvedValue(result);
    expect(await authController.login(user)).toEqual(result);
  });

  // it('should not validate user', async () => {
  //   const jwtStrategy = new JwtStrategy(authService);
  //   const payload: JwtPayload = { email: 'test@tessssst.com', roles: [{id: 0, name: 'USER'}]};
  //   const result = {
  //     statusCode: 401,
  //     error: 'Unauthorized',
  //   };
  //   jest.spyOn(jwtStrategy, 'validate').mockRejectedValue(result);
  //   expect(await jwtStrategy.validate(payload)).toEqual(result);
  // });
});
