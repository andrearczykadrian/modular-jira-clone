import { AuthService } from '../auth.service';
import { Test, TestingModule } from '@nestjs/testing';
import { UserDTO } from '../../user/dto/user.dto';
import { UserRO } from '../../user/dto/userRO.dto';
import { LoginDTO } from '../../user/dto/login.dto';
import { JwtStrategy } from '../jwt.strategy';
import { JwtPayload } from '../dto/jwt-payload.dto';
jest.mock('../auth.service');

describe('AuthService', () => {
  let authService: AuthService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
      ],
    }).compile();
    authService = module.get(AuthService);
  });

  it('should be created', () => {
    expect(authService).toBeDefined();
  });

  it('should register and respond with userRO', async () => {
    const user: UserDTO = {
      login: 'adrianadrian',
      email: 'adrian@gmail.com',
      password: 'testtest',
    };
    const result: UserRO = {
        login: 'adrianadrian',
        email: 'adrian@gmail.com',
        created: new Date(Date.now()),
        lastLogin: null,
        roles: [{id: 0, name: 'USER'}],
    };
    jest.spyOn(authService, 'register').mockResolvedValue(result);
    expect(await authService.register(user)).toEqual(result);
  });

  it('should login and respond with userRO and token', async () => {
    const user: LoginDTO = {
      login: 'adrianadrian',
      password: 'testtest',
    };
    const result: UserRO = {
      login: 'adrianadrian',
      email: 'adrian@gmail.com',
      created: new Date(Date.now()),
      lastLogin: null,
      roles: [{id: 0, name: 'USER'}],
      token: 'testtoken',
    };
    jest.spyOn(authService, 'login').mockResolvedValue(result);
    expect(await authService.login(user)).toEqual(result);
  });

  it('should validate user', async () => {
      const jwtStrategy = new JwtStrategy(authService);
      const payload: JwtPayload = { email: 'test@tessssst.com', roles: [{id: 0, name: 'USER'}]};
      const result = {
        login: 'adrianadrian',
        email: 'adrian@gmail.com',
      };
      jest.spyOn(jwtStrategy, 'validate').mockResolvedValue(result);
      expect(await jwtStrategy.validate(payload)).toEqual(result);
  });
  // it('should adasdasd user', async () => {
  //   const jwtStrategy = new JwtStrategy(authService);
  //   const payload: JwtPayload = { email: 'test@tessssst.com', roles: [{id: 0, name: 'USER'}]};
  //   const result = {
  //     statusCode: 401,
  //     error: 'Unauthorized',
  //   };
  //   jest.spyOn(jwtStrategy, 'validate').mockRejectedValue(result);
  //   expect(await jwtStrategy.validate(payload)).toEqual(result);
  // });
});
