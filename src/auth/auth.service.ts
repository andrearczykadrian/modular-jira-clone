import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './dto/jwt-payload.dto';
import { UserDTO } from '../user/dto/user.dto';
import { UserRO } from '../user/dto/userRO.dto';
import { Role } from '../user/entities/role.entity';
import { Roles } from '../user/enums/rolesEnum';
import { LoginDTO } from '../user/dto/login.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../user/entities/user.entity';
import { UserRepository } from '../user/user.repository';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly userRepository: UserRepository, private readonly jwtService: JwtService) {}

  createToken(email: string, roles: Role[]) {
    const user: JwtPayload = { email, roles };
    return this.jwtService.sign(user);
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    const { email }  = payload;
    return await this.userRepository
      .createQueryBuilder('user')
      .where('user.email = :email', { email })
      .innerJoin('user.roles', 'roles')
      .addSelect('roles.name')
      .getOne();
  }

  async register(data: UserDTO): Promise<UserRO> {
    const { login } = data;
    let user = await this.userRepository.findOne({ where: { login } });
    if (user) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }
    user = await this.userRepository.create(data);
    const role = new Role();
    role.id = Roles.ADMIN;
    role.name = Roles[Roles.ADMIN];
    user.roles = [role];
    await this.userRepository.save(user);
    return this.userResponseObject(user);
  }

  async login(data: LoginDTO): Promise<UserRO> {
    const { login, password } = data;
    const user = await this.userRepository
      .createQueryBuilder('user')
      .where('user.login = :login', { login })
      .innerJoin('user.roles', 'roles')
      .addSelect('roles.name')
      .getOne();
    if (!user || !(await user.comparePassword(password))) {
      throw new HttpException(
        'Invalid username/password',
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.userResponseObject(user, true);
  }

  private userResponseObject(user: User, withToken: boolean = false): UserRO {
    const { login, email, created, lastLogin, roles} = user;
    const responseObject: UserRO = { login, email, created, lastLogin, roles };
    if (withToken) {
      responseObject.token = this.createToken(email, roles);
    }
    return responseObject;
  }
}
