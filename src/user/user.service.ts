import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: UserRepository,
  ) {}

  // ADMIN
  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  // REGISTERED
  async findOne(id: number): Promise<User> {
    return await this.userRepository.findOne(id);
  }

  // OWN PROFILE - REGISTERED
  // TODO
}
