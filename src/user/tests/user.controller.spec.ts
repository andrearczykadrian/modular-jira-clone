import { UserService } from '../user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserController } from '../user.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../entities/user.entity';
import { AuthModule } from '../../auth/auth.module';

describe('User Controller', () => {
  let userService: UserService;

  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useValue: {},
        },
      ],
    }).compile();
    userService = module.get<UserService>(UserService);
  });
  it('should be defined', () => {
    const controller: UserController = module.get<UserController>(UserController);
    expect(controller).toBeDefined();
  });
});
