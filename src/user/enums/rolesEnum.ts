export enum Roles {
  USER = 0,
  ADMIN = 1,
  PROJECT_MANAGER = 2,
}
