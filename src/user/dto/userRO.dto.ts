import { Role } from '../entities/role.entity';

export class UserRO {
  login: string;
  email: string;
  created: Date;
  lastLogin: Date;
  roles: Role[];
  token?: string;
}
