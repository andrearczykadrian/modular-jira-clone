import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';

export class UserDTO {
  @IsNotEmpty()
  @MinLength(6)
  login: string;

  @IsNotEmpty()
  @MinLength(8)
  password: string;

  @IsEmail()
  email: string;
}
