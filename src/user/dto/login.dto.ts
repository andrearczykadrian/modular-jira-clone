import { IsNotEmpty, MinLength } from 'class-validator';

export class LoginDTO {
  @IsNotEmpty()
  @MinLength(6)
  login: string;

  @IsNotEmpty()
  @MinLength(8)
  password: string;
}
