import { BaseEntity, BeforeInsert, Column, CreateDateColumn, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Role } from './role.entity';
import * as bcrypt from 'bcrypt';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 40, unique: true })
  login: string;

  @Column('varchar', { length: 60})
  email: string;

  @Column('varchar', { length: 60 })
  password: string;

  @CreateDateColumn()
  created: Date;

  @Column({ default: null })
  lastLogin: Date;

  @ManyToMany(() => Role, { cascade: ['insert'] })
  @JoinTable()
  roles: Role[];

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.password);
  }
}
