import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';
import { UserService } from '../user/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from '../auth/jwt.strategy';
import { AuthService } from '../auth/auth.service';

let app: INestApplication;
const userService = { find: () => [{ statusCode: 401, error: 'Unauthorized' }] };

beforeAll(async () => {
  const module = await Test.createTestingModule({
    imports: [
      UserModule,
      AuthModule,
      TypeOrmModule.forRoot({host: 'localhost'}),
      PassportModule.register({ defaultStrategy: 'jwt' }),
      JwtModule.register({
        secretOrPrivateKey: 'secretKey',
        signOptions: {
          expiresIn: 3600,
        },
      }),
    ],
    providers: [AuthService, JwtStrategy],
  })
    .overrideProvider(UserService)
    .useValue(userService)
    .compile();

  app = module.createNestApplication();
  await app.init();
});

describe('Users', () => {
  it(`/GET users`, () => {
    return request(app.getHttpServer())
      .get('/users')
      .expect(401)
      .expect(userService.find()[0]);
  });

  it(`/GET users/specific_user`, () => {
    return request(app.getHttpServer())
      .get('/users/1')
      .expect(401)
      .expect(userService.find()[0]);
  });
});
